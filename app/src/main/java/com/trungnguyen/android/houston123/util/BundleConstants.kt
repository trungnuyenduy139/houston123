package com.trungnguyen.android.houston123.util

/**
 * Created by trungnd4 on 09/07/2018.
 */
object BundleConstants {
    //Login Screen
    const val USER_NAME = "user_name"
    const val PASSWORD = "password"

    //detail
    const val KEY_USER_DETAIL = "key_user_detail"
    const val KEY_UPDATE_USER_DETAIL = "key_update_user_detail"

    //List
    const val LIST_LECTURER_BUNDLE = "list_lecturer_bundle";
}
