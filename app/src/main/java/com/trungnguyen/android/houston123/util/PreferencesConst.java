package com.trungnguyen.android.houston123.util;

/**
 * Created by trungnd4 on 23/09/2018.
 */
public interface PreferencesConst {
    // login key
    String LOGIN_STATUS = "login-status";
    String ACCESS_TOKEN_PREF = "access_token_pref";
}
