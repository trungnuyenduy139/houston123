package com.trungnguyen.android.houston123.ui.main.personal;

/**
 * Created by trungnd4 on 06/10/2018.
 */
public interface IPersonalView {
    void navigateToLoginScreen();
}
